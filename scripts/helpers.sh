#!/bin/bash

log () { echo -e "$*"; return $? ; }

fail () { log "\nERROR: $*\n" ; exit 1 ; }

qpushd () {
	pushd "$1" &> /dev/null
}

qpopd () {
	popd &> /dev/null
}

cmd_exists () {
  type "$1" &> /dev/null;
}

getFile () {
	local output="$1"
	local url="$2"

	log "Retrieving file ${output} from ${url}"

	if cmd_exists wget
	then
		wget --quiet -O "$output" "$url"
	else
		curl --silent -o "$output" "$url"
	fi
	return $?
}

jarFile () {
    local name=$1
    local path=$2
    log "Creating zip for ${name}"
    if [ $OSTYPE == "cygwin" ]; then
        name="`cygpath -w $name`"
    fi
    if cmd_exists jar
    then
       jar Mcf "${name}.jar" ${path}
    else
       fail "jar command not found."
    fi
       
}

zipFile () {
	local name=$1
	local path=$2
	local jarname=$name
    if [ $OSTYPE == "cygwin" ]; then
        jarname="`cygpath -w $name`"
    fi
	jarFile "${jarname}" "${path}"
	mv ${name}.jar ${name}.zip
}

isHelpRequested() {
    for arg in $*
    do
    	if [ "$arg" = "help" -o "$arg" = "-help" -o "$arg" = "--help" -o "$arg" = "-h" ]
    	then
    		return 1
    	fi
    done
    return 0
}

DEPLOY_HELP_MESSAGE="  --deploy: specifies whether to do a mvn deploy:deploy-file instead of a mvn install:install-file. WARNING: this will permanently deploy that version to Maven, available to everyone. Only use this after making sure your artifact is valid."
CHROME_VERSION_WARNING="WARNING: for Chrome the latest stable browser will be retrieved, so make sure to provide the latest stable version."