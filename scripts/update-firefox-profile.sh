#!/bin/bash

source `dirname $0`/helpers.sh

isHelpRequested "$*"
if [ $? -ne 0 ]
then
    log "$0 <firebugversion> <version> [--deploy]"
	log "  firebugversion: valid version number of Firebug compatible with the Firefox version"
	log "  version: Firefox version as provided to update-browser.sh. Find your version at https://wiki.mozilla.org/Releases"
	log "  --deploy: specifies whether to do a mvn deploy:deploy-file instead of a mvn install:install-file. WARNING: this will permanently deploy given version to Maven, available to everyone"
	exit
fi

# The full version to download. eg. 1.7.3
if [ -z $1 ]; then
    echo "You need to provide Firebug version"
    exit 1
fi
if [ -z $2 ]; then
    echo "You need to provide Firefox version"
    exit 1
fi

firebugVersion=$1
firefoxVersion=$2
deploy=0

if [ "$3" = "--deploy" ]
then
    deploy=1
fi


addProfilePreferences () {
    outputDir=$1
    prefFile=${outputDir}/profile.preferences

    # Add move preferences below.
    echo "extensions.firebug.currentVersion=${firefoxVersion}" > ${prefFile}
    echo "extensions.firebug.console.enableSites=true" >> ${prefFile}
    echo "extensions.firebug.script.enableSites=true" >> ${prefFile}
}


major_version=`echo "${firebugVersion}" | cut -d'.' -f1,2`
url=https://getfirebug.com/releases/firebug/${major_version}/firebug-${firebugVersion}.xpi
output=/tmp/firebug.xpi
tmpdir=/tmp
if [ $OSTYPE == "cygwin" ]; then
    tmpdir="`cygpath -w $tmpdir`"
fi

if getFile ${output} ${url}
then

    qpushd /tmp
    profileDir=/tmp/firefox-profile
    mkdir -p ${profileDir}
    cp ${output} ${profileDir}

    addProfilePreferences "${profileDir}"

    qpushd "${profileDir}"
    echo "firefox profile version: ${firefoxVersion}" > ${profileDir}/profile.package
    echo "firebug version: ${major_version}" >> ${profileDir}/profile.package
    zipFile "${profileDir}" "*"
    qpopd
    jarFile /tmp/firefox-${firefoxVersion}-profile "firefox-profile.zip"
    qpopd

    if [ $deploy -eq 1 ]
    then
        mvn deploy:deploy-file -Dfile=${tmpdir}/firefox-${firefoxVersion}-profile.jar -DgroupId=com.atlassian.browsers -DartifactId=firefox-profile -Dversion=${firefoxVersion} -Dpackaging=jar -Durl=https://maven.atlassian.com/public -DrepositoryId=atlassian-public
    else

        mvn install:install-file -Dfile=${tmpdir}/firefox-${firefoxVersion}-profile.jar -DgroupId=com.atlassian.browsers -DartifactId=firefox-profile -Dversion=${firefoxVersion} -Dpackaging=jar
        log ""
        log "WARNING: This has been deployed locally to deploy to maven run with --deploy"

    fi

    log "Add the following to the pom.xml"
    log ""
    log "<dependency>"
    log "  <groupId>com.atlassian.browsers</groupId>"
    log "  <artifactId>firefox-profile</artifactId>"
    log "  <version>${firefoxVersion}</version>"
    log "</dependency>"

    rm ${output}
    rm -rf ${profileDir}
    rm /tmp/firefox-profile.zip
    rm /tmp/firefox-${firefoxVersion}-profile.jar

else
    echo "Failed to get Firebug ${firebugVersion}, check if it exists. Aborting"
    exit 1
fi

