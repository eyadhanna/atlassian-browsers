#!/bin/bash

# browser = firefox, chrome
# os = linux, linux64, osx
source `dirname $0`/helpers.sh

unpackTarBz2 () {
	local name=$1
	log "Unpacking ${name}.tar.bz2"
	bunzip2 "${name}.tar.bz2"
	tar -xf "${name}.tar"
	rm ${name}.tar
}

unpack7zip () {
    local name=$1
    local exename="${name}.exe"
    local szname="${name}.7z"

    if cmd_exists 7z; then
        local szcommand="7z"
    elif cmd_exists p7zip; then
        local szcommand="p7zip"
    fi

    if [ -z "$szcommand" ]; then
        fail "Unable to extract files from ${name}.exe\nInstall p7zip.\neg. brew install p7zip"
    fi

    mkdir $name
    qpushd $name
    if [ $szcommand = "7z" ]; then
        log "Unpacking $exename"
        $szcommand x -y "$exename" &> /dev/null || fail "Failed to extract 7zip binary: $exename"
    else
        # on Linux p7zip requires .7z suffix ...
        mv "$exename" "$szname"
        log "Unpacking $szname"
        $szcommand -d "$szname" &> /dev/null || fail "Failed to extract 7zip binary: $szname"
    fi

    qpopd
    rm -f "$exename"
    rm -f "$szname"
}

mountDmg () {
	local name=$1

	mount_point=${name}-mount
	mkdir ${mount_point}

	if cmd_exists hdiutil
	then
		log "Mounting ${name}.dmg"
		hdiutil attach "${name}.dmg" -mountpoint "${mount_point}" -quiet
		return "$?"
	else
		log "Converting .dmg to .img for mounting"
		if cmd_exists dmg2img
		then
			dmg2img -s "${name}.dmg" "${name}.img"
			sudo modprobe hfsplus
			sudo mount -t hfsplus -o loop "${name}.img" "${mount_point}"
			return "$?"
		else
			rm -rf "${mount_point}"
			fail "dmg2img not found, please install it\n  eg. sudo apt-get install dmg2img"
		fi
	fi
}

unmountDmg () {
	local name=$1

	mount_point=${name}-mount

	log "Unmounting ${mount_point}"

	if cmd_exists hdiutil
	then
		hdiutil detach "${mount_point}" -quiet
	else
		sudo umount "${mount_point}"
		rm "${name}.img"
	fi

	rm "${name}.dmg"
	rm -rf "${mount_point}"
}

untarLzma () {
	local file=$1
	tar --lzma -xf ${file} &> /dev/null
}

unpackDebPackage () {
	local name=$1

	ar x "${name}.deb" data.tar.lzma
	mkdir -p /tmp/tmpdeb
	mv data.tar.lzma /tmp/tmpdeb/.
	qpushd /tmp/tmpdeb

	log "Attempting to unpack deb package."
	if untarLzma /tmp/tmpdeb/data.tar.lzma
	then
		log "Deb package unpacked."
	else
		if cmd_exists 7z
		then
			7z -y x /tmp/tmpdeb/data.tar.lzma &> /dev/null
			tar -xf data.tar
			log "Deb package unpacked with 7z."
		else
			fail "Unable to extract the data.tar.lzma file from ${name}.deb\nInstall p7zip.\neg. brew install p7zip"
		fi
	fi

	qpopd

}

packageFirefoxForOS () {
    local os=$1
	local name=$2
	local version=$3

	case "$os" in
		linux | linux64)
			unpackTarBz2 $name
			qpushd firefox
			echo "Firefox for $os: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			rm -rf firefox
			;;
		osx)
			mountDmg $name || fail "Unable to mount ${name}.dmg, the downloaded image might be invalid"
			qpushd ${name}-mount/Firefox.app/
			mkdir ${name}
			cp -R * ${name}
			qpopd
			qpushd ${name}
			echo "Firefox for $os: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			unmountDmg $name
			rm -rf ${name}
			;;
        windows)
            unpack7zip $name
            qpushd $name/core
            echo "Firefox for $os: version - ${version}" > browser.package
            zipFile $name "*"
            qpopd
            rm -rf $name
            ;;
		*)
			fail "Unknown Operating System: ${1}"
			;;
	esac
}

packageChromeForOS () {
	local name=$2
	local version=$3
	case "$1" in
		linux | linux64)
			qpushd /tmp
			unpackDebPackage ${name}
			rm ${name}.deb
			qpushd /tmp/tmpdeb/opt/google/chrome
			echo "Chrome for $1: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			qpopd
			rm -rf /tmp/tmpdeb
			;;
		osx)
			mountDmg ${name}
			sleep 5
			pushd "${name}-mount/Google Chrome.app/"
			mkdir ${name}
			cp -R * ${name}
			qpopd
			qpushd ${name}
			echo "Chrome for osx: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			unmountDmg ${name}
			rm -rf ${name}
			;;
		*)
			fail "Unknown Operating System: ${1}"
			;;
	esac

}

packagePhantomJSForOS () {
	local os=$1
	local name=$2
	local version=$3

	case "$os" in
		linux | linux64)
			unpackTarBz2 $name
			qpushd $name
			echo "PhantomJS for $os: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			rm -rf $name
			;;
		osx | windows)
			unzip $name > /dev/null
			qpushd $name
			echo "PhantomJS for $os: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			rm -rf $name
			;;
		*)
			fail "Unknown Operating System: ${1}"
			;;
	esac
}

getFirefoxForOS () {
    local os=$1
	local version=$2
    local name=/tmp/firefox-${version}-$os

	case "$os" in
		linux)
			file=${name}.tar.bz2
			url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/linux-i686/en-US/firefox-${version}.tar.bz2"
			;;
		linux64)
			file=${name}.tar.bz2
        	url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/linux-x86_64/en-US/firefox-${version}.tar.bz2"
			;;
		osx)
			file=${name}.dmg
			url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/mac/en-US/Firefox%20${version}.dmg"
			;;
		windows)
		    file=${name}.exe
		    url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/win32/en-US/Firefox%20Setup%20${version}.exe"
		    ;;
		*)
			fail "Invalid Operating system ${os}"
			;;
	esac

	getFile "${file}" "${url}" || fail "Failed to retrieve firefox from ${url} and/or store in ${file}"
	packageFirefoxForOS $os $name ${version}
	log "Browser has been packaged: ${name}.zip"

}

getChromeForOS () {
	local version=$2
	name=/tmp/chrome-${version}-$1
	case "$1" in
		linux)
			file=${name}.deb
			url="https://dl.google.com/linux/direct/google-chrome-stable_current_i386.deb"
			;;
		linux64)
			file=${name}.deb
			url="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
			;;
		osx)
			file=${name}.dmg
			url="https://dl.google.com/chrome/mac/stable/CHFA/googlechrome.dmg"
			;;
		*)
			fail "Invalid Operating system ${1}"
			;;

	esac

	getFile "${file}" "${url}"
	packageChromeForOS $1 $name ${version}
	log "Browser has been packaged: ${name}.zip"
}

getPhantomJSForOS () {
	local os=$1
	local version=$2
	local name=/tmp/phantomjs-${version}-$os

	case "$os" in
		linux)
            name=/tmp/phantomjs-${version}-linux-i686
		    file=${name}.tar.bz2
			url="https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${version}-linux-i686.tar.bz2"
			;;
		linux64)
		    name=/tmp/phantomjs-${version}-linux-x86_64
		    file=${name}.tar.bz2
        	url="https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${version}-linux-x86_64.tar.bz2"
			;;
		osx)
		    name=/tmp/phantomjs-${version}-macosx
		    file=${name}.zip
			url="https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${version}-macosx.zip"
			;;
		windows)
		    file=${name}.zip
		    url="https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${version}-windows.zip"
		    ;;
		*)
			fail "Invalid Operating system ${os}"
			;;
	esac

	getFile "${file}" "${url}"
	packagePhantomJSForOS $os $name ${version}
	log "Browser has been packaged: ${name}.zip"

}

getBrowserForOs () {
  qpushd /tmp
  local os=$2
  local version=$3
  case "$1" in
	firefox)
		getFirefoxForOS $os $version
		;;
	chrome)
		getChromeForOS $os $version
		;;
    phantomjs)
        getPhantomJSForOS $os $version
        ;;
	*)
		fail "Invalid Browser ${1}"
		;;
  esac
}

isHelpRequested "$*"
if [ $? -ne 0 ]
then
	log "$0 <browser> <os> <version>"
	log "  browser: chrome,firefox,phantomjs"
	log "  os: linux,osx,windows"
	log "  version: any version number of the browser to get. $CHROME_VERSION_WARNING"
	exit
fi

browser=$1
os=$2
version=$3

getBrowserForOs $browser $os $version
