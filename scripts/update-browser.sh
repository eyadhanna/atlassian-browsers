#!/bin/bash

# update-browser retrieves a browser using browser-get and then deploys this to maven.
# add "help" argument to get help

source `dirname $0`/helpers.sh

isHelpRequested "$*"
if [ $? -ne 0 ]
then
    log "$0 <browser> <os> <version> [--deploy]"
	log "  browser: chrome,firefox,phantomjs"
	log "  os: linux,osx,windows"
	log "  version: any version number of the browser to update. $CHROME_VERSION_WARNING"
	log "$DEPLOY_HELP_MESSAGE"
	exit
fi

browser=$1
os=$2
version=$3
deploy=0

if [ "$4" = "--deploy" ]
then
    deploy=1
fi

path=`dirname "$0"`
tmpdir=/tmp
if [ $OSTYPE == "cygwin" ]; then
    tmpdir="`cygpath -w $tmpdir`"
fi

if cmd_exists mvn
then
  # do nothing
  echo ""
else
  fail "mvn command not found"
fi

if ${path}/browser-get.sh $browser $os $version
then

    qpushd /tmp
    major_version=`echo "${version}" | cut -d'.' -f1,2`
    mv "/tmp/${browser}-${version}-${os}.zip" "/tmp/${browser}-${os}.zip"
    jarFile "/tmp/${browser}-${major_version}-${os}" "${browser}-${os}.zip"
    qpopd

    if [ $deploy -eq 1 ]
    then
        mvn deploy:deploy-file -Dfile=${tmpdir}/${browser}-${major_version}-${os}.jar -DgroupId=com.atlassian.browsers -DartifactId=${browser} -Dversion=${version} -Dpackaging=jar -Dclassifier=${os} -Durl=https://maven.atlassian.com/public -DrepositoryId=atlassian-public
    else
        mvn install:install-file -Dfile=${tmpdir}/${browser}-${major_version}-${os}.jar -DgroupId=com.atlassian.browsers -DartifactId=${browser} -Dversion=${version} -Dpackaging=jar -Dclassifier=${os}
        log ""
        log "WARNING: This has been deployed locally to deploy to maven run with --deploy"
    fi

    log "Add the following to the atlassian-browsers-${os} pom.xml"
    log ""
    log "<dependency>"
    log "  <groupId>com.atlassian.browsers</groupId>"
    log "  <artifactId>${browser}</artifactId>"
    log "  <version>${version}</version>"
    log "  <classifier>${os}</classifier>"
    log "</dependency>"


    ## Clean up
    rm /tmp/${browser}-${os}.zip
    rm /tmp/${browser}-${major_version}-${os}.jar


else
    echo "FAILED to update browser. See error."
    exit 1
fi
