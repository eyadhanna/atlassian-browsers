Atlassian Browsers
==================

## Summary ##

This is a small Java library that allows mavenized projects to automatically pick up appropriate browser artifacts
(Chrome, Firefox, PhantomJS) depending on the local operating system and architecture.

## Usage ##

### Obtaining a browser artifact ###

TODO

### Upgrading to the latest browsers ###

1. Check the latest version of the browser:

 - Chrome: http://googlechromereleases.blogspot.com.au/
 - Firefox: https://wiki.mozilla.org/Releases
 - PhantomJS: http://phantomjs.org/releases.html

2. Run `scripts/update-browser.sh` for given version/OS combination, to get and package (and optionally deploy) the browser binaries. Run `scripts/update-browser.sh -help` for details.

3. Run `scripts/update-<browser>-profile.sh` to get and package (and optionally deploy) the browser profile.  Run `scripts/update-<browser>-profile.sh -help` for details.

4. Repeat for all browsers you want to deploy.

5. Update properties in the main `pom.xml` to upgrade to the newly deployed versions:

```
<chrome.version>29</chrome.version>
<chrome.profile.version>2.3</chrome.profile.version>
<firefox.version>23.0.1</firefox.version>
<firefox.profile.version>${firefox.version}</firefox.profile.version>
<ie.profile.version>2.35.3</ie.profile.version>
```