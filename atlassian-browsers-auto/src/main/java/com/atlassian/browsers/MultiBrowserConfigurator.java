package com.atlassian.browsers;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * <p/>
 * Maintains a collection of delegate {@link InstallConfigurator}s for given browser types, and invokes the
 * one corresponding to the browser type given to its {@link #setupBrowser(BrowserConfig)} method.
 *
 * <p/>
 * WARNING: the deprecated {@link #setupFirefoxBrowser(BrowserConfig)} and {@link #setupChromeBrowser(BrowserConfig)}
 * methods are not used by this class and not delegated to.
 *
 * @since 2.5
 */
public final class MultiBrowserConfigurator extends AbstractInstallConfigurator
{

    public static class Builder
    {
        private final Map<BrowserType, InstallConfigurator> configurators = Maps.newHashMap();

        public Builder addConfigurator(BrowserType type, InstallConfigurator configurator)
        {
            this.configurators.put(type, configurator);
            return this;
        }

        public MultiBrowserConfigurator build()
        {
            return new MultiBrowserConfigurator(this.configurators);
        }
    }

    private final Map<BrowserType,InstallConfigurator> configurators;

    public MultiBrowserConfigurator(@Nonnull Map<BrowserType, InstallConfigurator> configurators)
    {
        this.configurators = ImmutableMap.copyOf(configurators);
    }



    @Override
    public void setupBrowser(@Nonnull BrowserConfig browserConfig)
    {
        final InstallConfigurator configurator = configurators.get(browserConfig.getBrowserType());
        if (configurator != null)
        {
            configurator.setupBrowser(browserConfig);
        }
    }

}
