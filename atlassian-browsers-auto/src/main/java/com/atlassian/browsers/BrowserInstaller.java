package com.atlassian.browsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;

import static com.atlassian.browsers.Utils.extractZip;
import static com.atlassian.browsers.Utils.resourceExists;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.io.FileUtils.readFileToString;

/**
 * Defines the browser version, OS and the executable path for each browser version.
 */
enum BrowserInstaller
{
    FIREFOX_OSX(BrowserType.FIREFOX, OS.OSX, "Contents/MacOS/firefox-bin"),
    CHROME_OSX(BrowserType.CHROME, OS.OSX, "Contents/MacOS/Google Chrome"),

    FIREFOX_LINUX(BrowserType.FIREFOX, OS.LINUX, "firefox-bin"),
    CHROME_LINUX(BrowserType.CHROME, OS.LINUX, "chrome"),

    FIREFOX_LINUX64(BrowserType.FIREFOX, OS.LINUX64, "firefox-bin"),
    CHROME_LINUX64(BrowserType.CHROME, OS.LINUX64, "chrome"),

    FIREFOX_WINDOWS(BrowserType.FIREFOX, OS.WINDOWS, "firefox.exe"),

    IE_WINDOWS(BrowserType.IE, OS.WINDOWS, null);

    private static final Logger log = LoggerFactory.getLogger(BrowserInstaller.class);


    private static final String PACKAGE_MANIFEST = "profile.package";
    private static final String BROWSER_MANIFEST = "browser.package";

    private final BrowserType browser;
    private final OS os;
    private final String binaryPath;

    private BrowserInstaller(BrowserType browser, @Nullable OS os, @Nullable String binaryPath)
    {
        this.browser = checkNotNull(browser, "browser");
        this.os = checkNotNull(os, "os");
        this.binaryPath = binaryPath;
    }

    public BrowserType getBrowser()
    {
        return browser;
    }

    /**
     * OS this installer applies to. {@code null} signifies an that all operating systems are supported.
     *
     * @return {@code OS} supported by this installer
     */
    @Nullable
    public OS getOS()
    {
        return os;
    }


    public String getOsDirName()
    {
        return os == null ? OS.ALL : os.getName();
    }

    /**
     * Path to the browser executable within the browser package. May be {@literal null}, which means that this
     * installer does not support actually starting the target browser.
     *
     * @return path to the browser executable
     */
    @Nullable
    public String getBinaryPath()
    {
        return binaryPath;
    }

    /**
     * Determines the BrowserInstaller based on the browserStr passed and the current OS.
     * @param browserStr the browserStr that defines the BrowserInstaller that is needed.
     * @return The browser installer if it exists otherwise null.
     */
    public static BrowserInstaller typeOf(String browserStr)
    {
        OS os = OS.getType();
        BrowserType browserType = BrowserType.typeOf(browserStr);
        for (BrowserInstaller installer: BrowserInstaller.values())
        {

            if (installer.getOS().getName().equals(os.getName()) &&
                    installer.getBrowser().equals(browserType))
            {
                // Check it's not a more specific browser string such as firefox:path=/path/to/firefox-binary
                if (browserStr.equals(installer.getBrowser().getName()) ||
                    browserStr.matches("^" + installer.getBrowser().getName() + "-[0-9.]+$"))
                {
                    log.debug("Using {}", installer);
                    return installer;
                }
            }
        }
        log.info("There are no browser installers for browser string {}", browserStr);
        return null;
    }

    /**
     * Installs the current browser into the destination directory specified by
     * extracting the browser zip file.
     * If there is a profile zip for the browser it will also extract this.
     * Then the correct permissions are applied to the required files.
     *
     * @param destination the location to extract the browser into. This is the parent directory for the browser.
     * @param installConfigurator a default install configurator
     */
    public void install(File destination, BuiltInConfigurator installConfigurator)
    {
        String browserName = browser.getName();
        String binaryPath = getBinaryPath();
        String osDirName = getOsDirName();
        String profileName = browserName + "-profile";

        final String browserResource = "/" + browserName + "-" + osDirName + ".zip";
        final String profileResource = "/" + profileName + ".zip";
        final String osProfileResource = "/" + profileName + "-" + osDirName + ".zip";

        try
        {
            final File browserDir = extractBrowser(destination, browserResource);
            final File browserBinary = getBrowserBinary(binaryPath, browserDir);
            final File browserProfile = extractProfile(destination, profileResource, osProfileResource);
            BrowserConfig browserConfig = new BrowserConfig(browserDir, browserBinary, browserProfile, browser);
            installConfigurator.setupBrowser(browserConfig);
        }
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private File extractProfile(File destination, String profileResource, String osProfileResource) throws IOException
    {
        if (resourceExists(osProfileResource))
        {
            return extractZipProfileAndLog(destination, osProfileResource);
        }
        else if (resourceExists(profileResource))
        {
            return extractZipProfileAndLog(destination, profileResource);
        }
        else
        {
            log.info("Profile for {}:{} is not supported", getBrowser().name(), getOsDirName());
            return null;
        }
    }

    private File extractZipProfileAndLog(File destination, String osProfileResource) throws IOException
    {
        final File profileDir = extractZip(destination, osProfileResource);
        log.info("Installing {}", readFileToString(new File(profileDir, PACKAGE_MANIFEST)));
        return profileDir;
    }

    private File extractBrowser(File destination, String browserResource) throws IOException
    {
        if (!resourceExists(browserResource))
        {
            log.info("Browser {}:{} is not supported", getBrowser().name(), getOsDirName());
            return null;
        }
        final File browserDir = extractZip(destination, browserResource);
        log.info("Installing {}", readFileToString(new File(browserDir, BROWSER_MANIFEST)));
        return browserDir;
    }

    private File getBrowserBinary(String binaryPath, File browserDir) throws IOException
    {
        if (browserDir == null)
        {
            return null;
        }
        File browserBinary = new File(browserDir, binaryPath);
        if (!OsValidator.isWindows())
        {
            Utils.make755(browserBinary);
        }
        return browserBinary;
    }

}

