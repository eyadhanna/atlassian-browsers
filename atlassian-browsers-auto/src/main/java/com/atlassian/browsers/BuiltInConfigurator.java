package com.atlassian.browsers;

import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * The default browser configurator. Takes care of common configuration for particular browsers and delegates to the
 * {@link InstallConfigurator} provided by clients. This is so generic configuration can be done in the default
 * installer and then the client configurator is called to perform client specific configuration.
 *
 */
final class BuiltInConfigurator
{

    // at the moment only Chrome requires specific setup
    private final InstallConfigurator defaultInstallConfigurator = new MultiBrowserConfigurator.Builder()
            .addConfigurator(BrowserType.CHROME, BuiltInChromeConfigurator.INSTANCE)
            .addConfigurator(BrowserType.FIREFOX, NullConfigurator.INSTANCE)
            .addConfigurator(BrowserType.IE, NullConfigurator.INSTANCE)
            .build();
    private final InstallConfigurator clientConfigurator;

    protected BuiltInConfigurator(InstallConfigurator clientConfigurator)
    {
        this.clientConfigurator = checkNotNull(clientConfigurator, "clientConfigurator");
    }

    public void setupBrowser(BrowserConfig browserConfig)
    {
        defaultInstallConfigurator.setupBrowser(browserConfig);
        clientConfigurator.setupBrowser(browserConfig);
        // preserve backwards compatibility for now - assuming no one will implement both setupBrowser and setup<Specific>Browser
        switch (browserConfig.getBrowserType())
        {
            case FIREFOX:
                clientConfigurator.setupFirefoxBrowser(browserConfig);
                break;
            case CHROME:
                clientConfigurator.setupChromeBrowser(browserConfig);
                break;

        }
    }

    /**
     * Specific setup for Chrome - making sure all executable files have the right permissions on *nix systems.
     */
    private static class BuiltInChromeConfigurator extends AbstractInstallConfigurator
    {
        static final BuiltInChromeConfigurator INSTANCE = new BuiltInChromeConfigurator();

        // all known Chrome executable files to 755
        static final Iterable<String> CHROME_EXECUTABLES = ImmutableSet.of(
                "Google Chrome Helper",
                "chrome",
                "google-chrome"
        );

        @Override
        public void setupBrowser(@Nonnull BrowserConfig browserConfig)
        {
            try
            {
                if (!OsValidator.isWindows())
                {
                    if (browserConfig.getBrowserPath() != null)
                    {
                        for (File executableFile : Utils.findFiles(browserConfig.getBrowserPath(), CHROME_EXECUTABLES))
                        {
                            Utils.make755(executableFile);
                        }
                    }

                    String profilePath = browserConfig.getProfilePath();
                    if (profilePath != null)
                    {
                        File chromedriver = new File(profilePath, "chromedriver");
                        if (chromedriver.exists())
                        {
                            Utils.make755(chromedriver);
                        }
                    }

                }
            }
            catch(IOException e)
            {
                throw new RuntimeException(format("Unable to apply permissions to one of the Chrome executables: %s",
                        CHROME_EXECUTABLES), e);
            }
        }
    }
}
