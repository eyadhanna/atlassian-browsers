package com.atlassian.browsers;

import java.io.File;

/**
 * Defines the interface that each client has to implement to be used by the BrowserAutoInstaller.
 * This is so that each client can define the temporary location for installing the browsers as well
 * as the way to get the browserName
 * @see com.atlassian.browsers.BrowserAutoInstaller
 */
public interface BrowserConfiguration
{
    /**
     * The name of the browser. May be either one of the known 
     * {@link BrowserType browser type} strings, or a more specific 
     * browser string such as firefox:path=/path/to/firefox-binary
     * as defined by {@link BrowserInstaller#typeOf(String)}
     */
    String getBrowserName();

    /**
     * Where the {@link BrowserInstaller} installs the Browser if it is auto-installed.
     */
    File getTmpDir();
}
