package com.atlassian.browsers;

import javax.annotation.Nonnull;

/**
 * <p/>
 * Handles client-specific setup and configuration for a particular browser once the main setup has been done.
 *
 * <p/>
 * It has one generic method {@link #setupBrowser(BrowserConfig)} that should be overridden to
 * provide browser-specific configuration.
 *
 * <p/>
 * Consider using {@link com.atlassian.browsers.MultiBrowserConfigurator} to conveniently mix and match
 * simple install configurators for various browser types.
 *
 * <p/>
 * Otherwise consider extending {@link AbstractInstallConfigurator} instead of this class directly,
 * to protect your implementation from any changes in this API.
 */
public abstract class InstallConfigurator
{

    /**
     * Setup browser for given config.
     *
     * @param browserConfig browser configuration including the browser type
     */
    public void setupBrowser(@Nonnull BrowserConfig browserConfig)
    {
    }

    /**
     * <p/>
     * This method is deprecated and will be removed in 3.0. Use {@link AbstractInstallConfigurator} to avoid having
     * to implement this method.
     *
     * <p/>
     * WARN: if you retrofitted your configurator to implement the generic {@link #setupBrowser(BrowserConfig)} and
     * handle Firefox there, you should make this method a no-op, as both will be called by the framework
     *
     * @param browserConfig the browser config
     */
    @Deprecated
    public abstract void setupFirefoxBrowser(BrowserConfig browserConfig);

    /**
     * <p/>
     * This method is deprecated and will be removed in 3.0. Use {@link AbstractInstallConfigurator} to avoid having
     * to implement this method.
     *
     * <p/>
     * WARN: if you retrofitted your configurator to implement the generic {@link #setupBrowser(BrowserConfig)} and
     * handle Firefox there, you should make this method a no-op, as both will be called by the framework
     *
     * @param browserConfig the browser config
     */
    @Deprecated
    public abstract void setupChromeBrowser(BrowserConfig browserConfig);
}
