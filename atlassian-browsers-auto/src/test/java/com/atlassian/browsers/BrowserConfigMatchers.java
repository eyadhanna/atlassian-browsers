package com.atlassian.browsers;


import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import static org.hamcrest.CoreMatchers.equalTo;

public final class BrowserConfigMatchers
{

    private BrowserConfigMatchers()
    {
        throw new AssertionError("Don't instantiate me");
    }

    public static Matcher<BrowserConfig> withBrowserType(final BrowserType type)
    {
        return new FeatureMatcher<BrowserConfig, BrowserType>(equalTo(type), "browserType that", "browserType")
        {
            @Override
            protected BrowserType featureValueOf(BrowserConfig actual)
            {
                return actual.getBrowserType();
            }
        };
    }
}
